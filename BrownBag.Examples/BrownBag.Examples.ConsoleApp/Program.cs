﻿using BrownBag.Examples.Basics;
using BrownBag.Examples.ConsoleApp.Models;
using BrownBag.Examples.Data;
using System;
using System.Collections.Generic;

namespace BrownBag.Examples.ConsoleApp
{
    class Program
    {
        /// <summary>
        /// An example of a prompt data
        /// </summary>
        public static PromptData PromptData { get; set; }

        /// <summary>
        /// An example of a static constructor
        /// Note: There is no access modifier as this needs to always be 'public'
        /// </summary>
        static Program()
        {
            PromptData = new PromptData();
        }

        /// <summary>
        /// The Main Function
        /// This is where code begins execution on a Console Application
        /// </summary>
        /// <param name="args">Command line arguments, if any</param>
        static void Main(string[] args)
        {
            var stringExamples = new StringExamples();

            ShowInitialPromptAndCollectData();
            ShowPromptAndCollectData();

            while (PromptData.MethodChoiceValue != string.Empty)
            {
                // Switch example
                // Can not fall through between. 
                // Compiler will enforce having a break, return, or something else to jump over the next  after each case
                // Can have multiple cases use the same functionality though
                switch (PromptData.TypeChoiceValue)
                {
                    case 1:
                        Console.WriteLine(RunAMethod<StringExamples>(PromptData.MethodChoiceValue));
                        ShowPromptAndCollectData();
                        break;
                    case 2:
                        Console.WriteLine(RunAMethod<GenericsExamples>(PromptData.MethodChoiceValue));
                        ShowPromptAndCollectData();
                        break;
                    case 3:
                        Console.WriteLine(RunAMethod<LINQExamples>(PromptData.MethodChoiceValue));
                        ShowPromptAndCollectData();
                        break;
                    case 4:
                        Console.WriteLine(RunAMethod<OpenDataServiceExamples>(PromptData.MethodChoiceValue));
                        ShowPromptAndCollectData();
                        break;
                    case 5:
                        Console.WriteLine(RunAMethod<CSharpSixExamples>(PromptData.MethodChoiceValue));
                        ShowPromptAndCollectData();
                        break;
                    case 42:
                    default:
                        return;
                }
            }
        }

        /// <summary>
        /// Runs the specified method on the type in the TypeParam
        /// An example of generic usage
        /// </summary>
        /// <typeparam name="T">Accepts any type with a constructor</typeparam>
        /// <param name="methodName">Method name to run on the type specified</param>
        /// <returns>Value from the method invocation, if any. Otherwise, returns null.</returns>
        static object RunAMethod<T>(string methodName) where T : new()
        {
            // Everything inherits from object or can be boxed to an object
            // Can't use 'var' type identifier here due to the null value ambiguity
            object result = null;

            try
            {
                // Reflection usage here
                // 1. Gets the type of the type parameter
                // 2. Gets the method on the type specified
                // 3. Invokes the method on a new instance of the type in the type parameter specified with no method parameters (null)
                result = typeof(T).GetMethod(methodName).Invoke(new T(), null);
            }
            // Example of try/catch
            // Should always specify these clauses from most specific to least specific
            // Everything inherits from System.Exception
            catch (NullReferenceException)
            {
                Console.WriteLine("Method name: '{0}' is invalid. Please try again.", methodName);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }

            return result;
        }

        /// <summary>
        /// Shows the initial prompt to request the set of examples to execute and sets the choice
        /// </summary>
        static void ShowInitialPromptAndCollectData()
        {
            PromptData.TypeChoiceValue = 0;

            while (PromptData.TypeChoiceValue == 0)
            {
                Console.WriteLine("Enter one of the following numeric values");
                List<string> typeNumberMappingDescriptions = new List<string>
                {
                    typeof(StringExamples).Name,
                    typeof(GenericsExamples).Name,
                    typeof(LINQExamples).Name,
                    typeof(OpenDataServiceExamples).Name,
                    typeof(CSharpSixExamples).Name
                };

                int currentCount = 1;
                foreach (string typeNumberMappingDescription in typeNumberMappingDescriptions)
                {
                    Console.WriteLine(currentCount.ToString() + ") for "+ typeNumberMappingDescription);
                    currentCount++;
                }

                try
                {
                    PromptData.TypeChoiceValue = int.Parse(Console.ReadLine());
                }
                catch (Exception)
                {
                    // Swallow exception since in loop
                }
            }
        }

        /// <summary>
        /// Shows the method choice prompt and sets the choice
        /// </summary>
        static void ShowPromptAndCollectData()
        {
            Console.WriteLine(Environment.NewLine + "Enter whitespace to exit or example method name to execute.");
            PromptData.MethodChoiceValue = Console.ReadLine();
        }
    }
}
