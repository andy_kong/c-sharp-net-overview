﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrownBag.Examples.Basics
{
    /// <summary>
    /// Base Class
    /// </summary>
    public class BaseCSharpSixClass
    {
        public string SomethingToModify { get; set; } = "BaseCSharpSixClass auto property initializer set this.";
        public virtual string SomethingToModify2 { get; set; } = "BaseCSharpSixClass auto property initializer set this.";

        public BaseCSharpSixClass()
        {
            SomethingToModify += " BaseCSharpSixClass() constructor added to this.";
            SomethingToModify2 += " BaseCSharpSixClass() constructor added to this.";
        }

        public BaseCSharpSixClass(int inputInteger)
        {

        }
    }

    /// <summary>
    /// Derived Class
    /// </summary>
    public class DerivedCSharpSixClass : BaseCSharpSixClass
    {
        public override string SomethingToModify2 { get; set; } = "DerivedCSharpSixClass auto property initializer set this.";
        public string AutoProperty1 { get; set; } = "DerivedCSharpSixClass auto property initializer set this.";

        public DerivedCSharpSixClass()
        {
            SomethingToModify += " DerivedCSharpSixClass() added to this."; // Base Initialization sets first, then Base Constructor executes, and then finally this executes
            SomethingToModify2 += " DerivedCSharpSixClass() added to this."; // Overridden, so auto property initialization sets first then this executes
            AutoProperty1 += " DerivedCSharpSixClass() added to this."; // Auto property initialization sets first then this executes
        }

        public DerivedCSharpSixClass(string inputString) : base() // base() is unnecessary here
        {
            SomethingToModify += " DerivedCSharpSixClass() added to this."; // Base Initialization sets first, then Base Constructor executes, and then finally this executes
            SomethingToModify2 += " DerivedCSharpSixClass() added to this."; // Overridden, so auto property initialization sets first then this executes
            AutoProperty1 += " DerivedCSharpSixClass() added to this."; // Auto property initialization sets first then this executes
        }

        public DerivedCSharpSixClass(int inputInteger) : base(inputInteger) // base() is unnecessary here
        {
            SomethingToModify += " DerivedCSharpSixClass() added to this."; // Base Initialization sets first, then Base Constructor executes, and then finally this executes
            SomethingToModify2 += " DerivedCSharpSixClass() added to this."; // Overridden, so auto property initialization sets first then this executes
            AutoProperty1 += " DerivedCSharpSixClass() added to this."; // Auto property initialization sets first then this executes
        }
    }

    /// <summary>
    /// Exmaples on C# 6.0 features and constructor execution order
    /// </summary>
    public class CSharpSixExamples
    {
        public Func<DerivedCSharpSixClass, string> OutputString = 
            (cssebc => string.Format("SomethingToModify value: {1}{0}{0}SomethingToModify2 value: {2}{0}{0}AutoProperty1 value: {3}{0}{0}", 
                Environment.NewLine, 
                cssebc.SomethingToModify, 
                cssebc.SomethingToModify2, cssebc.AutoProperty1
                ));

        public Func<DerivedCSharpSixClass, string> OutputString2 =
            (cssebc => $"SomethingToModify value: {cssebc.SomethingToModify}{Environment.NewLine}{Environment.NewLine} SomethingToModify2 value: {cssebc.SomethingToModify2}{Environment.NewLine}{Environment.NewLine} AutoProperty1 value: {cssebc.AutoProperty1}{Environment.NewLine}{Environment.NewLine}");

        /// <summary>
        /// Example to show how base/derived class constructors work with auto property initialization
        /// </summary>
        /// <returns></returns>
        public string E1()
        {
            var derivedClass = new DerivedCSharpSixClass();
            return OutputString(derivedClass);
        }

        /// <summary>
        /// Example to show no difference due to base() vs non base() usage. Also to show that the OutputString and OutputString2 lambdas are equivalent
        /// </summary>
        /// <returns></returns>
        public string E2()
        {
            var derivedClass = new DerivedCSharpSixClass("dummyString");
            return OutputString2(derivedClass);
        }

        /// <summary>
        /// Example to show differences when the base constructor is specified to be executed by the derived constructor that does nothing
        /// </summary>
        /// <returns></returns>
        public string E3()
        {
            var derivedClass = new DerivedCSharpSixClass(0);
            return OutputString(derivedClass);
        }
    }
}
