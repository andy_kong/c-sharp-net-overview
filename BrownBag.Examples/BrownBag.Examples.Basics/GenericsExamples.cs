﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace BrownBag.Examples.Basics
{
    public class GenericsExamples
    {
        /// <summary>
        /// Shows generics behavior in C#/.NET
        /// Specifically, it shows that I can pass in anything that is a struct for the first parameter 
        /// and anything that is a IEnumerable collection for the second parameter and still get output without compiler errors
        /// </summary>
        /// <returns>Returns a string depicting generics behavior in C#/.NET</returns>
        public string E1()
        {
            return GenericToStringGenerator(10.4m, new List<string>
            {
                "first",
                "second",
                "third"
            })
            + Environment.NewLine
            + GenericToStringGenerator(DateTime.Now, new Dictionary<int, string>
            {
                { 1, "one" },
                { 2, "two" },
                { 3, "three" }
            });
        }

        /// <summary>
        /// Generic (but not really that generic) ToString generator method
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="T2"></typeparam>
        /// <param name="structToString">Struct type to ToString</param>
        /// <param name="collectionToString">IEnumerable collection type to ToString</param>
        /// <returns>A string representing the combined ToString of the struct and collection. 
        /// The string will also contain the default value of the struct type used</returns>
        public string GenericToStringGenerator<T, T2>(T structToString, T2 collectionToString)
            // Generic constraints
            // T must be a struct (which has a constructor, i.e. new T() is allowed)
            // T2 must be an IEnumerable (which allows us to use foreach)
            where T : struct
            where T2 : IEnumerable
        {
            StringBuilder collectionAsString = new StringBuilder();
            foreach (var item in collectionToString)
            {
                collectionAsString.Append(item.ToString());
                collectionAsString.Append(Environment.NewLine);
            }

            return string.Format("{0} {1}{2}{3}{4}{5}",
                "Default Value of Type(" + typeof(T).Name + "):", // Using T as a type here
                new T().ToString(), // Using T as a type here
                Environment.NewLine,
                structToString.ToString(),
                Environment.NewLine,
                collectionAsString.ToString());
        }
    }
}
