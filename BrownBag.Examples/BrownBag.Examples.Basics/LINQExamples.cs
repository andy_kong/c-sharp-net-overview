﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BrownBag.Examples.Data;

namespace BrownBag.Examples.Basics
{
    public class LINQExamples
    {
        /// <summary>
        /// Fluent Syntax LINQ Example
        /// </summary>
        /// <returns></returns>
        public string E1()
        {
            var openDataServiceExamples = new OpenDataServiceExamples();
            var openDataChicagoStreets = openDataServiceExamples.GetOpenDataChicagoStreets(openDataServiceExamples.RetrieveOpenDataChicagoStreetData().Result);

            var allTheStreets = openDataChicagoStreets
                .Where(odcs => odcs.direction == "N")
                .Take(10)
                .Aggregate("", (workingSentence, next) => workingSentence + next.full_street_name + Environment.NewLine);

            return allTheStreets;
        }

        /// <summary>
        /// Comprehension Syntax LINQ Example
        /// </summary>
        /// <returns></returns>
        public string E2()
        {
            var openDataServiceExamples = new OpenDataServiceExamples();
            var openDataChicagoStreets = openDataServiceExamples.GetOpenDataChicagoStreets(openDataServiceExamples.RetrieveOpenDataChicagoStreetData().Result);

            var allTheStreets = (from street in openDataChicagoStreets
                                 where street.direction == "N"
                                 select street)
                .Take(10) // No equivalent in query/comprehension syntax (http://stackoverflow.com/questions/17890729/how-can-i-write-take1-in-query-syntax)
                .Aggregate("", (workingSentence, next) => workingSentence + next.full_street_name + Environment.NewLine); // No equivalent in query/comprehension syntax

            return allTheStreets;
        }
    }
}
